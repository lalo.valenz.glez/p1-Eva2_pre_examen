//
//  AsignaturasViewController.swift
//  Eva2_pre_examen
//
//  Created by TEMPORAL2 on 09/11/16.
//  Copyright © 2016 TEMPORAL2. All rights reserved.
//

import UIKit

class AsignaturasViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var materias = ["Fundamentos de programacion", "Programacion Orientada a Objetos", "Estructura de Datos", "Topicos Avanzados de Programacion", "Graficacion", "Plataforma 1", "Plataforma 2", "Programacion Web", "Programacion Logica y Funcional", "Sistemas Programables", "Fundamentos de Bases de Datos", "Taller de Bases de Datos", "Administracion de Bases de Datos", "Fundamentos de Telecomunicaciones", "Redes de Computadoras", "Conmutacion y Enrutamiento de Datos", "Administracion de Redes", "Aplicaciones Web Para Dispositivos Moviles"]
    
    var semestres = ["1 Semestre",
                     "2 Semestre",
                     "3 Semestre",
                     "4 Semestre",
                     "5 Semestre",
                     "6 Semestre",
                     "7 Semestre",
                     "8 Semestre",
                     "9 Semestre"]
    
    
    @IBOutlet weak var lblMateria: UILabel!
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        let celda = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        celda.textLabel?.text = materias[indexPath.row]
        
        return celda
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return materias.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let materia = materias[(indexPath as NSIndexPath).row]
        lblMateria.text = materia
        let index = (indexPath as NSIndexPath).row
        let semestre_index = semestre(index)
        if(semestre_index != -1){
            let semes = semestres[semestre_index]
            let alerta = UIAlertController(title: "Semestre", message: "\(semes)", preferredStyle: .Alert)
            let action = UIAlertAction(title: "ok", style: .Destructive, handler: nil)
            alerta.addAction(action)
            presentViewController(alerta, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func semestre(index: Int) ->Int {
        var sem = -1
        switch(index){
        case 0:
            sem = 0
            break
        case 1:
            sem = 1
            break
        case 2:
            sem = 2
            break
        case 3:
            sem = 3
            break
        case 4:
            sem = 4
            break
        case 5:
            sem = 7
            break
        case 6:
            sem = 8
            break
        case 7:
            sem = 7
            break
        case 8:
            sem = 7
            break
        case 9:
            sem = 6
            break
        case 10:
            sem = 3
            break
        case 11:
            sem = 4
            break
        case 12:
            sem = 5
            break
        case 13:
            sem = 4
            break
        case 14:
            sem = 5
            break
        case 15:
            sem = 6
            break
        case 16:
            sem = 7
            break
        case 17:
            sem = 8
            break
        default: sem = -1
            
        }
        return sem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
